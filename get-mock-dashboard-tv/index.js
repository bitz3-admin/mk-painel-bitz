module.exports = async function (context, req) {
  let accountAmountNumber = parseInt(req.query.actualAmount || 1);
  let accumulatedDay =
    req.query.accumulatedDayNumber === "0"
      ? 830
      : parseInt(req.query.accumulatedDayNumber);

  const randomValue = Math.floor(Math.random() * (1000 - 0) + 0);
  accountAmountNumber =
    accountAmountNumber === 1 ? randomValue : accountAmountNumber;
  const diffPercent = Math.floor(
    ((randomValue - accountAmountNumber) / accountAmountNumber) * 100
  );

  const diffLastDayPercent = Math.floor(
    ((accumulatedDay + randomValue - 4570) / 4570) * 100
  );

  const diffLast7DayPercent = Math.floor(
    ((accumulatedDay + randomValue - 3784) / 3784) * 100
  );

  const dashboardData = {
    dashboardHourData: {
      ableAccountNumber: randomValue,
      lastHourPercentDiff: Math.abs(diffPercent).toFixed(0),
      lastHourAmount: accountAmountNumber,
      lastHourGrowed: diffPercent > 0 ? true : false,
    },
    dashboardDayData: {
      ableAccountNumber: accumulatedDay + randomValue,
      lastDayPercentDiff: Math.abs(diffLastDayPercent),
      lastDayAmount: 4570,
      lastDayGrowed: diffLastDayPercent > 0 ? true : false,
      last7DaysPercentDiff: Math.abs(diffLast7DayPercent),
      last7DaysAmount: 3784,
      last7DaysGrowed: diffLast7DayPercent > 0 ? true : false,
    },
  };

  context.res = {
    status: 200 /* Defaults to 200 */,
    headers: {
      "Access-Control-Allow-Credentials": "true",
      "Access-Control-Allow-Origin": "*", // Or the origins you want to allow requests from
      "Content-Type": "application/json",
    },
    body: dashboardData,
  };
};
